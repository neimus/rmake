package makefile

import (
	"bufio"
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
	"strings"
	"time"

	"gitlab.com/neimus/rmake/internal/command"
	"gitlab.com/neimus/rmake/internal/config"
	"gitlab.com/neimus/rmake/internal/hash"
)

type Makefile struct {
	appConfig config.AppConfig
	hash      hash.Hash
	command   command.Command

	indexFile string
}

func New(appConfig config.AppConfig) (*Makefile, error) {
	m := &Makefile{
		appConfig: appConfig,
		hash:      hash.Hash{IncludeDir: appConfig.IncludeDir},
		command:   command.Command{},
		indexFile: path.Join(appConfig.IncludeDir, appConfig.IndexFile),
	}
	if err := m.command.Check(); err != nil {
		return nil, err
	}
	return m, nil
}

func (m *Makefile) IsTimeUpdate(group string) (bool, error) {
	lastUpdate, err := m.hash.LastUpdated(group)
	if err != nil {
		return false, err
	}
	if time.Now().Sub(lastUpdate) <= m.appConfig.UpdateIncludeEvery {
		return false, nil
	}

	return true, nil
}

func (m *Makefile) RemoteHashFile(repo config.RemoteRepository) (string, error) {
	var (
		remoteHash string
		err        error
	)
	if repo.GitUri == "" {
		return time.Now().String(), nil
	}

	if remoteHash, err = m.command.GitLsRemote(repo.GitUri, repo.BranchName); err != nil {
		return "", err
	}

	return remoteHash, nil
}

func (m *Makefile) ChangedByHash(group string, remoteHash string) (bool, error) {
	localHash, err := m.hash.Read(group)
	if err != nil {
		return false, err
	}

	if remoteHash == localHash {
		return false, nil
	}
	return true, nil
}

func (m *Makefile) UpdateHash(group string, remoteHash string) error {
	if err := m.hash.Update(group, remoteHash); err != nil {
		return err
	}
	return nil
}

func (m *Makefile) DownloadFromUrl(group, url, fileName string) error {
	if err := m.createDirForGroup(group, fileName); err != nil {
		return err
	}
	dstPath := path.Join(m.appConfig.IncludeDir, group, fileName)

	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("file download failed: %s", resp.Status)
	}

	file, err := os.Create(dstPath)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = io.Copy(file, resp.Body)
	return err
}

func (m *Makefile) DownloadFromGit(group, fileName string, repo config.RemoteRepository) error {
	if err := m.createDirForGroup(group, fileName); err != nil {
		return err
	}
	dstPath := path.Join(m.appConfig.IncludeDir, group, fileName)

	return m.command.GitArchiveFile(repo.GitUri, repo.BranchName, dstPath, fileName)
}

func (m *Makefile) CreateIndex() error {
	var err error
	if _, err = os.Stat(m.indexFile); err != nil && os.IsNotExist(err) {
		f, err := os.Create(m.indexFile)
		if err != nil {
			return err
		}
		_, err = f.WriteString(fmt.Sprintf(".DEFAULT_GOAL := all\n\n.PHONY: all\nall:\n\t@%s\n\n", os.Args[0]))
		if err != nil {
			return err
		}

		if err = f.Close(); err != nil {
			return err
		}
	}
	return nil
}

func (m *Makefile) UpdateIndex(group, fileName string) error {
	file, err := os.OpenFile(m.indexFile, os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		return err
	}
	defer func() {
		if err != nil {
			_ = file.Close()
			return
		}
		err = file.Close()
	}()

	scanner := bufio.NewScanner(file)

	pathFile := path.Join(m.appConfig.IncludeDir, group, fileName)
	for scanner.Scan() {
		if strings.Contains(scanner.Text(), pathFile) {
			return nil
		}
	}

	err = scanner.Err()
	if err != nil {
		return err
	}

	_, err = file.WriteString(fmt.Sprintf("include %s\n", pathFile))
	return err
}

func (m *Makefile) createDirForGroup(group, fileName string) error {
	dir, _ := path.Split(fileName)
	if dir != "" {
		dir = strings.Trim(dir, "/")
	}

	return os.MkdirAll(path.Join(m.appConfig.IncludeDir, group, dir), 0777)
}
