package command

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"strings"
)

var (
	ErrExecGitLsRemote   = errors.New("error when running the 'git ls-remoter' command")
	ErrExecGitArchive    = errors.New("error when running the 'git archive' command")
	ErrEmptyResponse     = errors.New("empty response when running the 'git ls-remoter' command")
	ErrIncorrectResponse = errors.New("incorrect response from the command")
	ErrGitNotFound       = errors.New("git not found")
	ErrTarNotFound       = errors.New("tar not found")
	ErrEmptyData         = errors.New("empty data when retrieving a file with the 'git archive' command")
	ErrFailedUpdate      = errors.New("failed to update file with the 'git archive' command")
)

type Command struct {
}

func (c *Command) Check() error {
	if _, err := exec.LookPath("git"); err != nil {
		return ErrGitNotFound
	}
	if _, err := exec.LookPath("tar"); err != nil {
		return ErrTarNotFound
	}
	return nil
}

func (c *Command) GitLsRemote(gitUri, branchName string) (string, error) {
	gitLsCmd := exec.Command("git", "ls-remote", gitUri, branchName)
	output, err := gitLsCmd.Output()
	if err != nil {
		return "", fmt.Errorf("%w: %s", ErrExecGitLsRemote, err)
	}

	if len(output) == 0 {
		return "", ErrEmptyResponse
	}
	remoteHash := strings.Split(string(output), "\t")
	if len(remoteHash) == 0 {
		return "", fmt.Errorf("%w ('git ls-remote'): %s", ErrIncorrectResponse, output)
	}
	return remoteHash[0], nil
}

func (c *Command) GitArchiveFile(gitUri, branchName, dstPath, fileName string) error {
	file, err := os.Create(dstPath)
	if err != nil {
		return fmt.Errorf("%w: %s", ErrExecGitArchive, err)
	}
	defer func() {
		if err != nil {
			err = fmt.Errorf("%w: %s", ErrExecGitArchive, err)
			_ = file.Close()
			return
		}
		err = file.Close()
	}()

	var data bytes.Buffer
	r, w := io.Pipe()

	gitArchiveCmd := exec.Command("git", "archive", "--remote", gitUri, branchName, fileName)
	gitArchiveCmd.Stdout = w

	tarCmd := exec.Command("tar", "-xO")
	tarCmd.Stdin = r
	tarCmd.Stdout = &data

	if err = gitArchiveCmd.Start(); err != nil {
		return err
	}
	if err = tarCmd.Start(); err != nil {
		return err
	}

	if err = gitArchiveCmd.Wait(); err != nil {
		return err
	}
	if err = w.Close(); err != nil {
		return err
	}

	if err = tarCmd.Wait(); err != nil {
		return err
	}

	if data.Len() == 0 {
		return ErrEmptyData
	}

	var n int64
	if n, err = io.Copy(file, &data); err != nil {
		return err
	}
	if err = r.Close(); err != nil {
		return err
	}

	if n == 0 {
		return ErrFailedUpdate
	}

	return nil
}
