package app

import (
	"errors"
	"fmt"
	"os"
	"path"

	"gitlab.com/neimus/rmake/internal/config"
	"gitlab.com/neimus/rmake/internal/logger"
	"gitlab.com/neimus/rmake/internal/makefile"
	"gitlab.com/neimus/rmake/internal/sanitize"
)

var (
	ErrCreateIncludeDir = errors.New("error when creating a directory for rmake includes")
	ErrCreateIndexFile  = errors.New("error when creating an index file")
	ErrUpdateIndexFile  = errors.New("error when updating an index file")
)

type App struct {
	config   *config.Config
	makefile *makefile.Makefile
	logger   *logger.Logger
}

func New(logger *logger.Logger, config *config.Config) (*App, error) {
	m, err := makefile.New(config.App)
	if err != nil {
		return nil, err
	}

	return &App{
		makefile: m,
		config:   config,
		logger:   logger,
	}, nil
}

func (a *App) Run() error {
	if len(a.config.Repositories) == 0 {
		a.logger.Debug("Empty list of repositories. Nothing to update")
		return nil
	}

	var (
		lastErr      error
		countUpdated int
	)
	for group, repo := range a.config.Repositories {
		group = sanitize.Sanitize(group)
		a.logger.Debug("Starting a group update", logger.Field{"group": group})
		ok, err := a.makefile.IsTimeUpdate(group)
		if err != nil {
			a.logger.Err(
				"Error when checking the last file update time",
				logger.Field{"group": group},
				logger.Field{"error": err},
			)
			lastErr = err
			continue
		} else if !ok {
			a.logger.Debug("It's not time for an update yet", logger.Field{"group": group})
			continue
		}

		a.logger.Debug("Get the hash from the repository...",
			logger.Field{"git_uri": repo.GitUri},
			logger.Field{"branch_name": repo.BranchName},
			logger.Field{"files": repo.Files},
		)
		remoteHash, err := a.makefile.RemoteHashFile(repo)
		if err != nil {
			a.logger.Err("Failed to get hash from repository",
				logger.Field{"group": group},
				logger.Field{"git_uri": repo.GitUri},
				logger.Field{"branch_name": repo.BranchName},
				logger.Field{"files": repo.Files},
				logger.Field{"error": err},
			)
			lastErr = err
			continue
		}

		isChanged, err := a.makefile.ChangedByHash(group, remoteHash)
		if err != nil {
			a.logger.Err("Hash comparison error",
				logger.Field{"group": group},
				logger.Field{"remote_hash": remoteHash},
				logger.Field{"error": err},
			)
			lastErr = err
			continue
		} else if !isChanged {
			a.logger.Debug("The hash matches, skip this group and update the hash", logger.Field{"group": group})
			if err = a.makefile.UpdateHash(group, remoteHash); err != nil {
				a.logger.Err("Error when updating hash",
					logger.Field{"group": group},
					logger.Field{"remote_hash": remoteHash},
					logger.Field{"error": err},
				)
				lastErr = err
			}
			continue
		}

		for _, filePath := range repo.Files {
			a.logger.Debug(
				"Uploading a file to be included in the Makefile...",
				logger.Field{"group": group},
				logger.Field{"file_path": filePath},
			)

			fileName := filePath
			if repo.GitUri == "" {
				_, fileName = path.Split(filePath)
				err = a.makefile.DownloadFromUrl(group, filePath, fileName)
			} else {
				err = a.makefile.DownloadFromGit(group, fileName, repo)
			}
			if err != nil {
				a.logger.Err("Error downloading the file",
					logger.Field{"group": group},
					logger.Field{"git_uri": repo.GitUri},
					logger.Field{"branch_name": repo.BranchName},
					logger.Field{"file_name": fileName},
					logger.Field{"error": err},
				)
				lastErr = err
				continue
			}

			a.logger.Debug(
				"Updating the index for a downloaded file...",
				logger.Field{"group": group},
				logger.Field{"file_name": fileName},
			)
			if err = a.makefile.UpdateIndex(group, fileName); err != nil {
				return fmt.Errorf("%w: %s", ErrUpdateIndexFile, err)
			}
			a.logger.Info(
				"The inclusion file in the Makefile has been successfully updated",
				logger.Field{"group": group},
				logger.Field{"file_name": fileName},
			)
			countUpdated++
		}

		a.logger.Debug("Updating the hash group...", logger.Field{"group": group})
		if err = a.makefile.UpdateHash(group, remoteHash); err != nil {
			a.logger.Err("Error when updating hash",
				logger.Field{"group": group},
				logger.Field{"remote_hash": remoteHash},
				logger.Field{"error": err},
			)
			lastErr = err
			continue
		}
	}
	if countUpdated > 0 {
		a.logger.Info("The included Makefile files have been successfully updated")
	}

	return lastErr
}

func (a *App) ReInitialize() error {
	a.logger.Debug("Start re-initialization of folders and files for program operation")
	var err error
	for group := range a.config.Repositories {
		group = sanitize.Sanitize(group)
		a.logger.Debug("Removing the group...", logger.Field{"group": group})
		if err = os.RemoveAll(path.Join(a.config.App.IncludeDir, group)); err != nil {
			return err
		}
	}

	a.logger.Debug("Removing the index file...", logger.Field{"index_file": a.config.App.IndexFile})
	if err = os.RemoveAll(path.Join(a.config.App.IncludeDir, a.config.App.IndexFile)); err != nil {
		return err
	}

	return a.Initialize()
}

func (a *App) Initialize() error {
	a.logger.Debug("Start initialization of folders and files for program operation")
	var err error
	if err = os.MkdirAll(a.config.App.IncludeDir, 0777); err != nil {
		return fmt.Errorf("%w: %s", ErrCreateIncludeDir, err)
	}

	if err = a.makefile.CreateIndex(); err != nil {
		return fmt.Errorf("%w: %s", ErrCreateIndexFile, err)
	}

	return nil
}
