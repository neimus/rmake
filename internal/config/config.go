package config

import (
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
)

type (
	Config struct {
		App          AppConfig                   `json:"app" validate:"required"`
		Repositories map[string]RemoteRepository `json:"repositories,omitempty"`
	}

	AppConfig struct {
		IncludeDir         string        `json:"include_dir,omitempty" validate:"required"`
		IndexFile          string        `json:"index_file,omitempty"`
		UpdateIncludeEvery time.Duration `json:"update_include_every,omitempty"`
	}

	RemoteRepository struct {
		GitUri     string   `json:"git_uri,omitempty"`
		BranchName string   `json:"branch_name,omitempty"`
		Files      []string `json:"files,omitempty" validate:"required"`
	}
)

func Load(configPath string) (*Config, error) {
	viper.SetConfigFile(configPath)
	viper.SetConfigType("yaml")

	if err := viper.ReadInConfig(); err != nil {
		return nil, err
	}

	var config Config
	if err := viper.Unmarshal(&config, decodeJsonTag); err != nil {
		return nil, err
	}

	return &config, validator.New().Struct(&config)
}

func decodeJsonTag(decoderConfig *mapstructure.DecoderConfig) {
	decoderConfig.TagName = "json"
}
