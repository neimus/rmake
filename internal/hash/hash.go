package hash

import (
	"errors"
	"fmt"
	"io"
	"os"
	"path"
	"time"
)

const hashFile = "hash"

var (
	ErrUpdatingHash = errors.New("error when updating the hash")
	ErrReadingHash  = errors.New("error when reading the hash")
)

type Hash struct {
	IncludeDir string
}

func (h *Hash) Update(group, hash string) error {
	file, err := os.Create(path.Join(h.IncludeDir, group, hashFile))
	if err != nil {
		return fmt.Errorf("%w: %s", ErrUpdatingHash, err)
	}
	defer func() {
		if err != nil {
			err = fmt.Errorf("%w: %s", ErrUpdatingHash, err)
			_ = file.Close()
			return
		}
		err = file.Close()
	}()

	_, err = file.WriteString(hash)
	return err
}

func (h *Hash) Read(group string) (string, error) {
	file, err := os.Open(path.Join(h.IncludeDir, group, hashFile))
	if err != nil {
		// index file does not yet exist
		if os.IsNotExist(err) {
			err = nil
			return "", nil
		}
		return "", fmt.Errorf("%w: %s", ErrReadingHash, err)
	}
	defer func() {
		if err != nil {
			err = fmt.Errorf("%w: %s", ErrReadingHash, err)
		}
		_ = file.Close()
	}()

	var localHash []byte
	if localHash, err = io.ReadAll(file); err != nil {
		return "", err
	}

	return string(localHash), nil
}

func (h *Hash) LastUpdated(group string) (time.Time, error) {
	fInfo, err := os.Stat(path.Join(h.IncludeDir, group, hashFile))
	if err != nil {
		if os.IsNotExist(err) {
			return time.Time{}, nil
		}
		return time.Time{}, err
	}
	return fInfo.ModTime(), nil
}
