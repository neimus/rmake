package sanitize

import (
	"strings"
	"unicode"

	"golang.org/x/text/runes"
	"golang.org/x/text/transform"
)

func removeIllFormed(input string) string {
	output, _, _ := transform.String(runes.ReplaceIllFormed(), input)
	return output
}

func replaceNonAlphaNum(input string) string {
	replaceNonAlphaNumFunc := runes.Map(func(r rune) rune {
		if !unicode.Is(unicode.Latin, r) && !unicode.IsDigit(r) {
			return '_'
		}
		return r
	})
	output, _, _ := transform.String(replaceNonAlphaNumFunc, input)
	return output
}

func Sanitize(input string) string {
	return strings.TrimSpace(replaceNonAlphaNum(removeIllFormed(input)))
}
