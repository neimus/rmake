package logger

import (
	"fmt"
	"strings"
)

const (
	escape = "\x1b"
	end    = 0
)

// Foreground text colors
const (
	fgBlack int = iota + 30
	fgRed
	fgGreen
	fgYellow
	fgBlue
	fgMagenta
	fgCyan
	fgWhite
)

// Format text
const (
	reset int = iota
	bold
	faint
)

type (
	Field map[string]any

	Logger struct {
		Verbose bool
	}
)

func (l *Logger) Info(msg string, fields ...Field) {
	fmt.Println(
		l.wrap("[INFO]", fgGreen, bold),
		l.wrap(msg, fgGreen, reset),
		l.wrapFields(fields),
	)
}

func (l *Logger) Debug(msg string, fields ...Field) {
	if !l.Verbose {
		return
	}
	fmt.Println(
		l.wrap("[DEBUG]", fgYellow, bold),
		l.wrap(msg, fgYellow, reset),
		l.wrapFields(fields),
	)
}

func (l *Logger) Warn(msg string, fields ...Field) {
	fmt.Println(
		l.wrap("[WARN]", fgMagenta, bold),
		l.wrap(msg, fgMagenta, reset),
		l.wrapFields(fields),
	)
}

func (l *Logger) Err(msg string, fields ...Field) {
	fmt.Println(
		l.wrap("[ERR]", fgRed, bold),
		l.wrap(msg, fgRed, reset),
		l.wrapFields(fields),
	)
}

func (l *Logger) Msg(msg string, fields ...Field) {
	fmt.Println(
		l.wrap("[MSG]", fgBlue, bold),
		l.wrap(msg, fgBlue, reset),
		l.wrapFields(fields),
	)
}

func (l *Logger) wrapFields(fields []Field) string {
	if len(fields) == 0 {
		return ""
	}
	var buf strings.Builder
	buf.WriteString("\n\t{")

	for index, field := range fields {
		for name, value := range field {
			if index != 0 {
				buf.WriteString(", ")
			}
			_, _ = fmt.Fprintf(&buf, `"%s": %v`, name, value)
		}
	}

	buf.WriteString("}")

	return l.wrapFormatOnly(buf.String(), faint)
}

func (l *Logger) wrap(msg string, fgColor, format int) string {
	return fmt.Sprintf("%s[%d;%dm%s%s[%dm", escape, format, fgColor, msg, escape, end)
}

func (l *Logger) wrapFormatOnly(msg string, format int) string {
	return fmt.Sprintf("%s[%dm%s%s[%dm", escape, format, msg, escape, end)
}
