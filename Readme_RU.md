# Remote inclusion of a Makefile

[EN](Readme.md)

[[_TOC_]]

## Описание

Программное решение для подключения Makefile из других репозиториев или по прямой ссылке.
Вдохновленно другим решением https://github.com/tj/mmake
Из-за частичной не работоспособности с приватными репозиториями `mmake` и полной замены на команду make (из-за чего
пропадает, например autocomplete) было написано данное решение.
На данный момент `rmake` поддерживает включение файлов из gitlab и прямых ссылок из других источников.
Например, для github возможно подключения файла по прямой ссылке `https://raw.githubusercontent.com/<REPO>/<NAME>/<BRANCH_NAME>/<PATH_TO_FILE>`

## Возможности

- указание настроек и необходимых подключений в Makefile с помощью yaml
- встраивание `rmake` в Makefile для обновления включений. Что позволяет использовать исходную команду make
- указание времени, когда включение считается просроченным и необходимо обновление
- обновление происходить только когда хеш ветки был изменен (когда появились новые изменения)
- прямое скачивание включений по ссылке
- подробное логирование

## Зависимости

Для запуска необходимо установленные пакеты:

- git
- tar
- ssh

## Установка

```shell
go install gitlab.com/neimus/rmake@latest
```

## Конфигурация

`rmake` использует конфигурационный `yaml` файл. По умолчанию, он ищет в корне директории `Makefile.yaml`
Также можно указать другой `yaml` файл с помощью аргумента при запуске `--config`

```shell
rmake --config="Makefile.yaml"
```

Конфигурация состоит из двух групп `app` и `repositories`. В `app` указаны настройки программы:

```yaml
app:
  # Путь до папки с файлами включений
  include_dir: "remote/Makefile"
  # Наименование индексного файла, который необходимо добавить в основной Makefile. 
  # Он будет создан в папке с файлами включений `include_dir`
  index_file: "index.mk"
  # Время после которого необходимо обновить файл включений 
  # (время вычисляется на основе времени последнего обновления хеш-файла)
  update_include_every: 10m
```

В `repositories` указываются данные репозиториев из которых необходимо включение файлов в Makefile.
В качестве ключа используется уникальная группа (папка), в которой будут загружены все файлы из этого репозитория.
Внутри группы (папки) будет создан `hash` файл с хешом репозитория.
На основе даты создания/обновления этого файла рассчитывается параметр `update_include_every`

Пример:

```yaml
...
repositories:
  common-product:
    git_uri: "ssh://git@gitlab.com/common-product/app.git"
    branch_name: "BRANCH-85"
    files: [ "include-main.mk", "include-test.mk" ]
  second-repo:
    git_uri: "ssh://git@gitlab.com/second-repo/app.git"
    branch_name: "master"
    files: [ "second-include.mk" ]
```

В данном примере, необходимо из репозитория `ssh://git@gitlab.com/common-product/app.git`
подключить файлы `include-main.mk` и `include-test.mk` из ветки `BRANCH-85`.
И из репозитория `ssh://git@gitlab.com/second-repo/app.git` подключить файл `second-include.mk` из ветки `master`

Кроме того можно подключить файл по прямой ссылке без указания `git_uri` или `branch_name`

```yaml
  github-repo:
    files: [ "https://raw.githubusercontent.com/repo/name/master/include.mk" ]
```

Итоговая конфигурация может выглядеть примерно так:

```yaml
app:
  include_dir: "remote/Makefile"
  index_file: "index.mk"
  update_include_every: 10m
repositories:
  common-product:
    git_uri: "ssh://git@gitlab.com/common-product/app.git"
    branch_name: "BRANCH-85"
    files: [ "include-main.mk", "include-test.mk" ]
  second-repo:
    git_uri: "ssh://git@gitlab.com/second-repo/app.git"
    branch_name: "master"
    files: [ "second-include.mk" ]
  github-repo:
    files: [ "https://raw.githubusercontent.com/repo/name/master/include.mk" ]
```

## Запуск

После создания конфигурации, необходимо запустить инициализацию структуры папок и файлов
(конфигурационный файл должен быть в текущей директории запуска):

```shell
rmake --init
```

Будут созданы папки по пути указанным в конфигурационном файле `include_dir`, а также файл `index_file`
Который необходимо подключить в свой Makefile

```shell
include remote/Makefile/index.mk
```

Также будут созданы групповые папки и подтянуты все зависимости.

Для дальнейшего обновления включений можно просто запускать команду

```shell
rmake
```

или использовать добавленную команду в индексном файле с переопределенным `.DEFAULT_GOAL := all`

```shell
make all
```

или

```shell
make
```

Если необходимо пересоздать всю структуру включений заново, можно воспользоваться командой

```shell
rmake --force-update
```

Для вывода подробных логов, используйте флаг `-v`

```shell
rmake -v
```

## Пример работы

Ознакомиться с примером работы rmake, можно по ссылке. https://gitlab.com/neimus/rmake-example
В примере подключаются файлы из этого репозитория из папки `example`


## TODO

- удаление включений, которые были удалены из конфигурации при обновлении
- добавить вывод версии
