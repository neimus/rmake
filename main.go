package main

import (
	"flag"
	"os"

	"gitlab.com/neimus/rmake/internal/app"
	"gitlab.com/neimus/rmake/internal/config"
	"gitlab.com/neimus/rmake/internal/logger"
)

func main() {
	var (
		configPath  string
		init        bool
		verbose     bool
		forceUpdate bool
	)
	flag.StringVar(&configPath, "config", "Makefile.yaml", "Path to the configuration file")
	flag.BoolVar(&init, "init", false, "Start creating the necessary files and folders for the program to work")
	flag.BoolVar(&forceUpdate, "force-update", false, "Re-download and update all included files in the Makefile")
	flag.BoolVar(&verbose, "v", false, "Enabling detailed logging")

	flag.Parse()

	log := &logger.Logger{Verbose: verbose}

	if configPath == "" {
		log.Err("The configuration path cannot be empty. Set it with the --config argument")
		os.Exit(1)
	}

	conf, err := config.Load(configPath)
	if err != nil {
		log.Err("Configuration file could not be loaded", logger.Field{"error": err})
		os.Exit(1)
	}

	app, err := app.New(log, conf)
	if err != nil {
		log.Err("Unable to run the program", logger.Field{"error": err})
		os.Exit(1)
	}

	if forceUpdate {
		if err = app.ReInitialize(); err != nil {
			log.Err("Error during ReInitialization of files and folders for program work", logger.Field{"error": err})
			os.Exit(1)
		}
	}

	if init {
		if err = app.Initialize(); err != nil {
			log.Err("Error during initialization of files and folders for program work", logger.Field{"error": err})
			os.Exit(1)
		}
	}

	if err = app.Run(); err != nil {
		log.Err("Failed to include remote Makefiles", logger.Field{"error": err})
		os.Exit(1)
	}
}
