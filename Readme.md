# Remote inclusion of a Makefile

[RU](Readme_RU.md)

[[_TOC_]]

## Description

Software solution to include Makefile from other repositories or by direct link.
Inspired by another solution https://github.com/tj/mmake
Because of the partial breakdown of the `mmake` private repositories and the complete substitution with the make
command (which causes
disappears, like autocomplete), this solution was written.
At this time, `rmake` supports including files from gitlab and direct links from other sources.
For example, when downloading from github, you can include the file by direct
link `https://raw.githubusercontent.com/<REPO>/<NAME>/<BRANCH_NAME>/<PATH_TO_FILE>`

## Features

- specifying settings and necessary inclusions in the Makefile using yaml
- embedding `rmake` in the Makefile to update the inclusions. Which allows you to use the original make command
- Specify a time when an inclusion is overdue and an upgrade is needed.
- the upgrade happens only when a branch hash has been changed (when new changes appear)
- direct download of the inclusions
- detailed logging

## Dependencies

You need to have packages installed to run it:

- git
- tar
- ssh

## Installation

```shell
go install gitlab.com/neimus/rmake@latest
```

## Configuration

`rmake` uses a configuration `yaml` file. By default, it looks for `Makefile.yaml` in the root directory.
You can also specify another `yaml` file with an argument when running `--config`

```shell
rmake --config="Makefile.yaml"
```

The configuration consists of two groups `app` and `repositories`. The `app` contains the program settings:

```yaml
app:
  # Path to the folder with inclusion files
  include_dir: "remote/Makefile"
  # The name of the index file to be added to the main Makefile. 
  # This will be created in the folder with the inclusion files `include_dir
  index_file: "index.mk"
  # Time after which the inclusion file must be updated 
  # (the time is calculated from the last time the hash file was updated)
  update_include_every: 10m
```

In `repositories` you specify the repository data from which you want to include files in the Makefile.
As a key, a unique group (folder) is used, in which all files from that repository will be loaded.
Within the group (folder), a `hash` file will be created with the repository hash.
Based on the creation/update date of this file, the `update_include_every` parameter is calculated

Example:

```yaml
...
repositories:
  common-product:
    git_uri: "ssh://git@gitlab.com/common-product/app.git"
    branch_name: "BRANCH-85"
    files: [ "include-main.mk", "include-test.mk" ]
  second-repo:
    git_uri: "ssh://git@gitlab.com/second-repo/app.git"
    branch_name: "master"
    files: [ "second-include.mk" ]
```

In this example, from the repository `ssh://git@gitlab.com/common-product/app.git` you need
include the files `include-main.mk` and `include-test.mk` from the `BRANCH-85` branch.
And from repository `ssh://git@gitlab.com/second-repo/app.git` include the file `second-include.mk` from branch `master`

It is also possible to attach a file by direct link without specifying `git_uri` or `branch_name`.

```yaml
  github-repo:
    files: [ "https://raw.githubusercontent.com/repo/name/master/include.mk" ]
```

The final configuration may look something like this:

```yaml
app:
  include_dir: "remote/Makefile"
  index_file: "index.mk"
  update_include_every: 10m
repositories:
  common-product:
    git_uri: "ssh://git@gitlab.com/common-product/app.git"
    branch_name: "BRANCH-85"
    files: [ "include-main.mk", "include-test.mk" ]
  second-repo:
    git_uri: "ssh://git@gitlab.com/second-repo/app.git"
    branch_name: "master"
    files: [ "second-include.mk" ]
  github-repo:
    files: [ "https://raw.githubusercontent.com/repo/name/master/include.mk" ]
```

## Launch

After creating the configuration, you must run the initialization of the folder and file structure
(the configuration file must be in the current startup directory):

```shell
rmake --init
```

Folders will be created according to the path specified in the configuration file `include_dir`, as well as the
file `index_file`.
Which must be included in your Makefile

```shell
include remote/Makefile/index.mk
```

Group folders will also be created and all dependencies will be pulled up.

To further update the inclusions, you can simply run the command

```shell
rmake
```

or use the added command in the index file with overridden `.DEFAULT_GOAL := all`

```shell
make all
```

or

```shell
make
```

If you want to recreate (force update) the whole structure of inclusions again, you can use the command

```shell
rmake --force-update
```

To display detailed logs, use the `-v` flag

```shell
rmake -v
```

## Example

To see an example of how rmake works, see this link: https://gitlab.com/neimus/rmake-example
The example includes files from this repository in the `example` folder

## TODO

- Removing inclusions that were deleted from the configuration during the update
- add version output
